package com.example.testhellondk;

import android.os.Bundle;
import android.app.Activity;
import android.app.NativeActivity;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	static {
		System.loadLibrary("mylib");
	}
	
	public native String getMyData();
	public native String sendString();
	TextView tv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle(getMyData());
		tv = (TextView)findViewById(R.id.tv1);
		String text1 = getMyData() + sendString(); 
		tv.setText(text1);
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
